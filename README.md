# REST API Mi inversion imbiliaria USAGE

This API is built to get statistical behavior data about real-estate market in spain.


## Get States info

Get all available spain states with them zones and points

### Request

`GET /states/`

    curl -i -H 'Accept: application/json' https://idealista-bd.herokuapp.com/states/

### Response

    HTTP/1.1 200 OK
    Date: Thu, 25 Mar 2020 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    [
        {
            "id": 1,
            "title": "Comunidad de Madrid",
            "zones": [
                {
                    "id": 11,
                    "title": "north",
                    "label": "Zona norte",
                    "points": [
                        {
                            "id": 112,
                            "name": "Sierra norte de madrid",
                            "villages": [
                                "Guadalix de la sierra",
                                "Soto del real",
                                "Manzanares del real",
                                "Miraflores de la sierra",
                                "Bustarviejo",
                                "La Cabrera",
                                "Torrelaguna"
                            ],
                            "center": "40.81572,-3.70105",
                            "distance": "8000"
                        }
                    ]
                }
            ]
        }
    ]

## Get historical information

Get timeline info of a spain state specified. This request return an array with an average data of size, price and price by area (price / m2) get it weekly.

### Request

`GET /flat/:stateId`

    curl -i -H 'Accept: application/json' https://idealista-bd.herokuapp.com/flats/1

### Response

    HTTP/1.1 200 OK
    Date: Thu, 25 Mar 2020 12:36:30 GMT
    Status: 200 OK
    Connection: close
    Content-Type: application/json
    [
        {
            "_id": "5e7c8f619aeb2e520c62cba4",
            "stateId": "1",
            "date": "26/03/2020 12:17:53",
            "totalFlats": 3,
            "avgPrice": 116633.33333333333,
            "avgPriceByArea": 1835.6666666666667,
            "avgSize": 66,
            "data": [
                {
                    "zoneId": 11,
                    "date": "26/03/2020 12:17:53",
                    "totalFlats": 3,
                    "avgPrice": 116633.33333333333,
                    "avgPriceByArea": 1835.6666666666667,
                    "avgSize": 66,
                    "points": [
                        {
                            "pointId": 112,
                            "date": "26/03/2020 12:17:53",
                            "totalFlats": 3,
                            "avgPrice": 116633.33333333333,
                            "avgPriceByArea": 1835.6666666666667,
                            "avgSize": 66
                        }
                    ]
                }
            ]
        },
        {
            "_id": "5e7c8f739aeb2e520c62cba5",
            "stateId": "1",
            "date": "26/03/2020 12:18:11",
            "totalFlats": 3,
            "avgPrice": 116633.33333333333,
            "avgPriceByArea": 1835.6666666666667,
            "avgSize": 66,
            "data": [
                {
                    "zoneId": 11,
                    "date": "26/03/2020 12:18:11",
                    "totalFlats": 3,
                    "avgPrice": 116633.33333333333,
                    "avgPriceByArea": 1835.6666666666667,
                    "avgSize": 66,
                    "points": [
                        {
                            "pointId": 112,
                            "date": "26/03/2020 12:18:11",
                            "totalFlats": 3,
                            "avgPrice": 116633.33333333333,
                            "avgPriceByArea": 1835.6666666666667,
                            "avgSize": 66
                        }
                    ]
                }
            ]
        }
    ]

## Create a new historical data

Post by especific spain state to create new avg data value in that state collection

### Request

`POST /create/flat/:stateId`

    curl -i -H 'Accept: application/json' https://idealista-bd.herokuapp.com/create/flat/1
### Response

    HTTP/1.1 201 Created
    Date: Thu, 25 Mar 2020 12:36:30 GMT
    Status: 201 Created
    Connection: close
    Content-Type: application/json
    Content-Length: 36

    {
        "stateId": "1",
        "date": "26/03/2020 12:28:16",
        "totalFlats": 3,
        "avgPrice": 116633.33333333333,
        "avgPriceByArea": 1835.6666666666667,
        "avgSize": 66,
        "data": [
            {
                "zoneId": 11,
                "date": "26/03/2020 12:28:16",
                "totalFlats": 3,
                "avgPrice": 116633.33333333333,
                "avgPriceByArea": 1835.6666666666667,
                "avgSize": 66,
                "points": [
                    {
                        "pointId": 112,
                        "date": "26/03/2020 12:28:16",
                        "totalFlats": 3,
                        "avgPrice": 116633.33333333333,
                        "avgPriceByArea": 1835.6666666666667,
                        "avgSize": 66
                    }
                ]
            }
        ],
        "_id": "5e7c91d0a6fad23b7c25ba45"
    }
