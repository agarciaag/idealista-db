import '@babel/polyfill';
import express, {json} from 'express'
import IndexRouter from './routes/index.routes'
import TokenRouter from './routes/token.routes'
import RawRouter from './routes/raw.routes'
import {llave} from './helpers/config'

export const app = express();
const http = require('http');

require("dotenv").config();
//Settings
app.set('port',process.env.PORT || 2727);
app.set('llave',llave);


//Midelwares
app.use(json());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET');
    res.header('Allow', 'GET');
    next();
});


//EndPoints
app.use('/',express.static(__dirname + '/statics/'));
app.use('/raw',RawRouter);
app.use('/token',TokenRouter);
app.use(`/api`,IndexRouter);



async function main() {
    app.listen(app.get('port'));
    console.log('Server on port',app.get('port'));
}

main();