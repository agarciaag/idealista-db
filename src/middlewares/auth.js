import {llave} from "../helpers/config";

const moment = require('moment');
const jwt = require('jsonwebtoken');


export function isAuth (req,res,next) {
    if(!req.headers['access-token']) {
        return res.status(403).send({message:"Token was not found"})
    }

    const token = req.headers['access-token'];
    const payload = jwt.verify(token,llave,function(err,decoded) {
        if (err) return res.status(401).send({message:"Invalid Token"});
        else {
            if(decoded.exp < moment().unix()){
                return res.status(401).send({message:"Expired token"})
            }
            req.user = decoded.user;
            next()
        }
    });

}

