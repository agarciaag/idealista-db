import MongoClient from 'mongodb';
import "dotenv/config";
import {bdName} from "./config"

export async function connect() {
    try {
        const client = await MongoClient.connect(process.env.MONGODB_URI,
            {useUnifiedTopology:true}
        );
        const db = client.db(bdName);
        console.log('DB is connected');
        return db;
    } catch (e) {
        console.log(e)
    }
}