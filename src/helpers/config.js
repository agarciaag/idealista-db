import "dotenv/config";


export const bdName = "heroku_c846g315";
export const flatsURL =process.env.FLATS_URL = process.env.FLATS_URL || 'https://idealista-server.herokuapp.com/flats';
export const rawFlatsURL ='https://idealista-server.herokuapp.com/raw/flats';
export const zonesURL ='https://idealista-server.herokuapp.com/zones';
export const llave = process.env.KEY;
export const user = process.env.USER;
export const pss  = process.env.PSS;
