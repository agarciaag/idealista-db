import axios from 'axios'
import {flatsURL,zonesURL,rawFlatsURL} from './config'


export const getFlats = (zoneId) => {
    return axios
        .get(`${flatsURL}/${zoneId}`)
        .then(res => {
            const flats = res.data;
            if (typeof flats === 'object') {
                console.log("recuperacion de los pisos correctamente");
                return flats;
            }
        })
        .catch(error => {
            console.log('error al recuperar los pisos', error);
        });
};

export const getZones = () => {
    return axios
        .get(zonesURL)
        .then(res => {
            const zones = res.data;
            if (typeof zones === 'object') {
                console.log("recuperacion de las zonas correctamente");
                return zones;
            }
        })
        .catch(error => {
            console.log('error al recuperar las zonas', error);
        });
};

export const getRawFlats = (zoneId) => {
    return axios
        .get(`${rawFlatsURL}/${zoneId}`)
        .then(res => {
            const flats = res.data;
            if (typeof flats === 'object') {
                console.log("recuperacion de los datos en bruto de los pisos correctamente");
                return flats;
            }
        })
        .catch(error => {
            console.log('error al recuperar los datos en bruto de los pisos', error);
        });
}