import {Router} from 'express';
import {getFlats, getZones} from '../helpers/actiosAPI'
import {connect} from "../helpers/database";

const auth = require('../middlewares/auth');
const moment =require('moment');
const router = Router();


//Post by especific spain state to create new avg data value in that state collection
router.post('/create/flat/:stateId', auth.isAuth ,async (req,res)=>{
    const {stateId} = req.params;
    const db = await connect();
    const states = await getZones();
    const state = states.find(item => Number(item.id) === Number(stateId));
    const flats =  await getFlats(stateId);
    const result = await db.collection(state.title).insertOne(flats);
    res.status(201).json(result.ops[0]);

});

// Get timeline info of a spain state specified. This request return an array with an average data of size, price and price by area (price / m2) get it weekly..
router.get('/flats/:stateId', async (req,res)=>{
    const {stateId} = req.params;
    const db = await connect();
    const states = await getZones();
    const state = states.find(item => Number(item.id) === Number(stateId));
    const result = await db.collection(state.title).find({}).toArray();
    res.status(200).json(result);
});


//Get all available spain states with them zones and points
router.get('/states' , async (req,res)=>{
    const states = await getZones();
    res.status(200).json(states)
});



export default router;