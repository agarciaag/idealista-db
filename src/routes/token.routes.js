import {Router} from 'express';
import {llave,user,pss} from "../helpers/config";


const router = Router();
const jwt =require('jsonwebtoken');


router.post('/', (req, res) => {
    if(req.headers.user === user && req.headers.password === pss) {
        const payload = {
            check:  true,
            user: req.headers.usuario
        };
        const token = jwt.sign(payload, llave, {
            expiresIn: 60 * 60 * 24
        });
        res.json({
            mensaje: 'Autenticación correcta',
            token: token
        });
    } else {
        res.json({ mensaje: "Usuario o contraseña incorrectos"})
    }
});

export default router;