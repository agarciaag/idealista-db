import {Router} from 'express';
import {getRawFlats,getZones} from '../helpers/actiosAPI'

const router = Router();

router.get('/flats/:stateId', async (req,res)=>{
    const {stateId} = req.params;
    const states = await getZones();
    const state = states.find(item => Number(item.id) === Number(stateId));
    if (!state){
        console.log("StateId provided doesn't match with any state configured")
        res.status(404).json("State Id provided does not match with any state inside the list")
    }else{
        const result = await getRawFlats(state.id)
        if (result.length===0){
            res.status(204).json({message:`There isn't data for this state (${state.title})`});
        }else {
            res.status(200).json(result);
        }

    }
});

export default router;