"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _config = require("../helpers/config");

var router = (0, _express.Router)();

var jwt = require('jsonwebtoken');

router.post('/', function (req, res) {
  if (req.headers.user === "agarciaag" && req.headers.password === "korex020692") {
    var payload = {
      check: true,
      user: req.headers.usuario
    };
    var token = jwt.sign(payload, _config.llave, {
      expiresIn: 60 * 60 * 24
    });
    res.json({
      mensaje: 'Autenticación correcta',
      token: token
    });
  } else {
    res.json({
      mensaje: "Usuario o contraseña incorrectos"
    });
  }
});
var _default = router;
exports["default"] = _default;