"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _actiosAPI = require("../helpers/actiosAPI");

var _database = require("../helpers/database");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var auth = require('../middlewares/auth');

var moment = require('moment');

var router = (0, _express.Router)(); //Post by especific spain state to create new avg data value in that state collection

router.post('/create/flat/:stateId', auth.isAuth, /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee(req, res) {
    var stateId, db, states, state, flats, result;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            stateId = req.params.stateId;
            _context.next = 3;
            return (0, _database.connect)();

          case 3:
            db = _context.sent;
            _context.next = 6;
            return (0, _actiosAPI.getZones)();

          case 6:
            states = _context.sent;
            state = states.find(function (item) {
              return Number(item.id) === Number(stateId);
            });
            _context.next = 10;
            return (0, _actiosAPI.getFlats)(stateId);

          case 10:
            flats = _context.sent;
            _context.next = 13;
            return db.collection(state.title).insertOne(flats);

          case 13:
            result = _context.sent;
            res.status(201).json(result.ops[0]);

          case 15:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function (_x, _x2) {
    return _ref.apply(this, arguments);
  };
}()); // Get timeline info of a spain state specified. This request return an array with an average data of size, price and price by area (price / m2) get it weekly..

router.get('/flats/:stateId', /*#__PURE__*/function () {
  var _ref2 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee2(req, res) {
    var stateId, db, states, state, result;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            stateId = req.params.stateId;
            _context2.next = 3;
            return (0, _database.connect)();

          case 3:
            db = _context2.sent;
            _context2.next = 6;
            return (0, _actiosAPI.getZones)();

          case 6:
            states = _context2.sent;
            state = states.find(function (item) {
              return Number(item.id) === Number(stateId);
            });
            _context2.next = 10;
            return db.collection(state.title).find({}).toArray();

          case 10:
            result = _context2.sent;
            res.status(200).json(result);

          case 12:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function (_x3, _x4) {
    return _ref2.apply(this, arguments);
  };
}()); //Get all available spain states with them zones and points

router.get('/states', /*#__PURE__*/function () {
  var _ref3 = _asyncToGenerator( /*#__PURE__*/regeneratorRuntime.mark(function _callee3(req, res) {
    var states;
    return regeneratorRuntime.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return (0, _actiosAPI.getZones)();

          case 2:
            states = _context3.sent;
            res.status(200).json(states);

          case 4:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));

  return function (_x5, _x6) {
    return _ref3.apply(this, arguments);
  };
}());
var _default = router;
exports["default"] = _default;