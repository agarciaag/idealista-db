"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isAuth = isAuth;

var _config = require("../helpers/config");

var moment = require('moment');

var jwt = require('jsonwebtoken');

function isAuth(req, res, next) {
  if (!req.headers['access-token']) {
    return res.status(403).send({
      message: "Token was not found"
    });
  }

  var token = req.headers['access-token'];
  var payload = jwt.verify(token, _config.llave, function (err, decoded) {
    if (err) return res.status(401).send({
      message: "Invalid Token"
    });else {
      if (decoded.exp < moment().unix()) {
        return res.status(401).send({
          message: "Expired token"
        });
      }

      req.user = decoded.user;
      next();
    }
  });
}