import express, { json } from 'express';
import IndexRouter from './routes/index.routes';
const app = express();

require("dotenv").config(); //Settings


app.set('port', process.env.PORT || 2727); //Midelwares

app.use(json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET');
  res.header('Allow', 'GET');
  next();
}); //Routes ej localhost:3110/flats/1

app.use(`/`, IndexRouter);