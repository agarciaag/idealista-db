"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.llave = exports.zonesURL = exports.flatsURL = exports.bdName = void 0;

require("dotenv/config");

var bdName = "heroku_c846g315";
exports.bdName = bdName;
var flatsURL = process.env.FLATS_URL = process.env.FLATS_URL || 'https://idealista-server.herokuapp.com/flats';
exports.flatsURL = flatsURL;
var zonesURL = 'https://idealista-server.herokuapp.com/zones';
exports.zonesURL = zonesURL;
var llave = "020692";
exports.llave = llave;