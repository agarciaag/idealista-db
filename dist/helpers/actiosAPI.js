"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getZones = exports.getFlats = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _config = require("./config");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var getFlats = function getFlats(zoneId) {
  return _axios["default"].get("".concat(_config.flatsURL, "/").concat(zoneId)).then(function (res) {
    var flats = res.data; //Taking just the required data

    if (_typeof(flats) === 'object') {
      console.log("recuperacion de los pisos correctamente");
      return flats;
    }
  })["catch"](function (error) {
    console.log('error al recuperar los pisos', error);
  });
};

exports.getFlats = getFlats;

var getZones = function getZones() {
  return _axios["default"].get(_config.zonesURL).then(function (res) {
    var zones = res.data; //Taking just the required data

    if (_typeof(zones) === 'object') {
      console.log("recuperacion de las zonas correctamente");
      return zones;
    }
  })["catch"](function (error) {
    console.log('error al recuperar las zonas', error);
  });
};

exports.getZones = getZones;